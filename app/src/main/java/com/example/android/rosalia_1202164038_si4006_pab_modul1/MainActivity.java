package com.example.android.rosalia_1202164038_si4006_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editpanjang, editlebar;
    Button buttonCek;
    TextView textViewHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editpanjang = (EditText) findViewById(R.id.editpanjang);
        editlebar   = (EditText) findViewById(R.id.editlebar);
        buttonCek   = (Button) findViewById(R.id.buttonCek);
        textViewHasil = (TextView) findViewById(R.id.textViewHasil);

        buttonCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String panjang,lebar;
                panjang = editpanjang.getText() .toString();
                lebar   = editlebar.getText() .toString();

                double p     = Double.parseDouble(panjang);
                double l     = Double.parseDouble(lebar);
                double hasil = p * l;
                textViewHasil.setText("" + hasil);
            }
        });
    }
}
